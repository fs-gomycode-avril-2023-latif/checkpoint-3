import React from "react";
import Player from "./Player";
import players from "../Players";
const PlayerList = () => {
  return (
    <div>
      <Player users={players} />
    </div>
  );
};

export default PlayerList;
