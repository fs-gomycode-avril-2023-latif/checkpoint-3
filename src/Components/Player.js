import React from "react";

import Button from "react-bootstrap/Button";
import Card from "react-bootstrap/Card";

const Player = ({ users }) => {
  console.log(users);
  return (
    <>
      <div className="d-flex no-wrap">
        {users.map((user, index) => (
          <Card key={index} className="mx-4" style={{ width: "18rem" }}>
            <Card.Img
              variant="top"
              src={user.pathImage}
              alt={`photo de ${user.nom}`}
              height="200px"
            />
            <Card.Body>
              <Card.Title>{user.nom}</Card.Title>
              <Card.Text>
                <ul>
                  <li>Equipe : {user.equipe}</li>
                  <li>Nationnalite : {user.nationalite}</li>
                  <li>
                    Numero de Maillot: <i>{user.numeroMaillot}</i>
                  </li>
                  <li>Age : {user.age}</li>
                </ul>
              </Card.Text>
            </Card.Body>
          </Card>
        ))}
      </div>
    </>
  );
};

export default Player;
