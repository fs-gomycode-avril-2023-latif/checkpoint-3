import React from "react";
// · Les attributs de chaque joueur sont le nom, l’équipe, la nationalité, le numéro de maillot, l’âge et une URL d’image pour le joueur.
import img1 from "./images/img1.jpg";
import img2 from "./images/img2.jpg";
import img3 from "./images/img3.jpg";
import img4 from "./images/img4.jpg";
const players = [
  {
    nom: "Kylian Mbappe",
    equipe: "PSG",
    nationalite: "Francaise",
    numeroMaillot: 7,
    age: 23,
    pathImage: img1,
  },
  {
    nom: "Cristiano Ronaldo",
    equipe: "Real Madrid",
    nationalite: "Portugais",
    numeroMaillot: 7,
    age: 33,
    pathImage: img2,
  },
  {
    nom: "Messi inconnu",
    equipe: "Miami",
    nationalite: "Argentine",
    numeroMaillot: 10,
    age: 32,
    pathImage: img3,
  },
  {
    nom: "Erling Haaland",
    equipe: "Man City",
    nationalite: "Norvégienne",
    numeroMaillot: 9,
    age: 21,
    pathImage: img4,
  },
];

export default players;
