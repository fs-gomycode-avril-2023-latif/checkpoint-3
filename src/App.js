import React from "react";

import EnTete from "./Components/EnTete";
import Player from "./Components/Player";
import players from "./Players";
import PlayerList from "./Components/PlayerList";

function App() {
  return (
    <>
      <EnTete />

      <PlayerList />
    </>
  );
}

export default App;
